/**
 * Copyright (c) 2013, Blessed Contraptions
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * -Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 *
 * -Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the
 *  distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTW ARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _MQTTE_H_
#define _MQTTE_H_

#include <stdint.h>
#include <stdbool.h>

#ifndef MQTTE_RX_BUF_SIZE
#define MQTTE_RX_BUF_SIZE 512
#endif

#ifndef MQTTE_TX_BUF_SIZE
#define MQTTE_TX_BUF_SIZE 512
#endif

#ifndef lock_t
typedef void* lock_t;
#endif

#define MQTTE_MSG_COUNT = 16
enum mqtte_message_type {
	MQTTE_MSG_TYPE_RESERVED    = 0,
	MQTTE_MSG_TYPE_CONNECT     = 1,
	MQTTE_MSG_TYPE_CONNACK     = 2,
	MQTTE_MSG_TYPE_PUBLISH     = 3,
	MQTTE_MSG_TYPE_PUBACK      = 4,
	MQTTE_MSG_TYPE_PUBREC      = 5,
	MQTTE_MSG_TYPE_PUBREL      = 6,
	MQTTE_MSG_TYPE_PUBCOMP     = 7,
	MQTTE_MSG_TYPE_SUBSCRIBE   = 8,
	MQTTE_MSG_TYPE_SUBACK      = 9,
	MQTTE_MSG_TYPE_UNSUBSCRIBE = 10,
	MQTTE_MSG_TYPE_UNSUBACK    = 11,
	MQTTE_MSG_TYPE_PINGREQ     = 12,
	MQTTE_MSG_TYPE_PINGRESP    = 13,
	MQTTE_MSG_TYPE_DISCONNECT  = 14,
	MQTTE_MSG_TYPE_RESERVED2   = 15,
};

enum mqtte_qos_level {
	MQTTE_QOS_FIRE_AND_FORGET      = 0,
	MQTTE_QOS_ACKNOWLEDGE_DELIVERY = 1,
	MQTTE_QOS_ASSURED_DELIVERY     = 2,
	MQTTE_QOS_RESERVED             = 3,
};

enum mqtte_platform_cb_type {
	MQTTE_PLATFORM_TX,
	MQTTE_PLATFORM_TX_INIT,
	MQTTE_PLATFORM_RX,
	MQTTE_PLATFORM_RX_INIT,
	MQTTE_PLATFORM_MS_TICK,
	MQTTE_PLATFORM_CREATE_LOCK,
	MQTTE_PLATFORM_TAKE_LOCK,
	MQTTE_PLATFORM_GIVE_LOCK,
};

enum mqtte_rx_state {
	MQTTE_RX_INIT,
	MQTTE_RX_SEEKING_LEN,
	MQTTE_RX_RECEIVING,
};

struct mqtte_session {
	char client_id[23];

	/* Com handle (socket/serial port/whatever). Shared with RX/TX */
	int com_handle;
	/* Rx related data */
	uint8_t rx_buf[MQTTE_RX_BUF_SIZE];
	uint16_t rx_index;
	lock_t rx_lock;
	uint32_t rx_required;
	enum mqtte_rx_state _rx_state;
	/* Tx related data */
	uint8_t tx_buf[MQTTE_TX_BUF_SIZE];
	uint16_t tx_index;
	lock_t tx_lock;
	/* Platform Callbacks */
	int(*tx_cb)(int, uint8_t*, int);
	int(*rx_cb)(int, uint8_t*, int);
	int(*get_ms_tick)(void);
	int(*lock_create)(lock_t lock);
	int(*lock_take)(lock_t lock);
	int(*lock_give)(lock_t lock);

	/* User Packet callbacks */
	void(*_connect_cb)(void);
	void(*_connack_cb)(uint8_t);
	void(*_puback_cb)(uint16_t);

};

void mqtte_init_session(struct mqtte_session *session);
void mqtte_start_session(struct mqtte_session *session,
			 int com_handle,
			 int(*com_tx)(int, uint8_t*, int),
			 int(*com_rx)(int, uint8_t*, int));
int mqtte_register_platform_callback(struct mqtte_session *session,
				     enum mqtte_platform_cb_type type,
				     void *cb);
int mqtte_register_msg_callback(struct mqtte_session *session,
				enum mqtte_message_type type,
				void *cb);
int mqtte_set_client_id(struct mqtte_session *session,
			const char *id);
int mqtte_connect(struct mqtte_session *session,
		  char *username,
		  char *password,
		  bool will_flag,
		  bool will_retain,
		  char *will_topic,
		  char *will_msg,
		  enum mqtte_qos_level will_qos,
		  uint16_t keepalive);
int mqtte_publish(struct mqtte_session *ses,
		  const char *topic_name,
		  uint8_t *payload,
		  int payload_len,
		  enum mqtte_qos_level qos,
		  bool dup,
		  bool retain,
		  uint16_t msg_id);
void mqtte_poll(struct mqtte_session *session);
int mqtte_set_keepalive(struct mqtte_session *session,
			uint16_t seconds);

#endif
