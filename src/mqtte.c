#include <mqtte.h>
#include <string.h>

#include <stdio.h>

/* Due to the way that mqtt calculates packet length, we need to make
 * sure that all receive buffers are initialized to 0xff, as such, it
 * will be impossible for the get_packet_length algorithm to
 * improperly calculate the length */
void mqtte_init_session(struct mqtte_session *session)
{
	if (!session)
		return;

	memset(session, 0x00, sizeof(struct mqtte_session));
	memset(session->rx_buf, 0xff, MQTTE_RX_BUF_SIZE);

	session->_rx_state = MQTTE_RX_INIT;
}

/* Inserts the encoded packet length (length) into the buffer *buf,
   returns the number of bytes consumed in the buffer*/
static int mqtte_set_packet_length(uint8_t *buf, uint32_t length)
{
	uint8_t digit;
	int i = 0;

	/* Prevent over length (as per spec)*/
	if (length > 268435455)
		return -1;

	do {
		digit = length % 128;
		length /= 128;
		if (length)
			digit |= 0x80;
		buf[i++] = digit;
	} while (length);
	return i;
}

/*  Input: an encoded packet length stored in *buf
    Return values:
    -1 - More bytes are required to properly decode this header and /
    or the header is corrupt
   >=0 - packet length */
static int mqtte_get_packet_length(uint8_t *buf)
{
	int mul = 1;
	int val = 0;
	uint8_t digit;
	int i = 0;

	do {
		digit = buf[i++];
		val += (digit & 127) * mul;
		mul *= 128;

	} while ((digit & 128) && i < 5);

	if (i >= 5) {
		return -1;
	}
	return val;
}

int mqtte_register_platform_callback(struct mqtte_session *session,
				     enum mqtte_platform_cb_type type,
				     void *cb)
{
	if (!cb || !session)
		return -1;

	switch (type) {
	case MQTTE_PLATFORM_TX:
		session->tx_cb = cb;
	case MQTTE_PLATFORM_RX:
		session->rx_cb = cb;
	case MQTTE_PLATFORM_MS_TICK:
		session->get_ms_tick = cb;
	default:
		return -2;
	}

	return 0;
}

int mqtte_register_msg_callback(struct mqtte_session *ses,
				enum mqtte_message_type type,
				void *cb)
{
	if (!cb || !ses)
		return -1;

	switch (type) {
	case MQTTE_MSG_TYPE_RESERVED:
	case MQTTE_MSG_TYPE_CONNECT:
	case MQTTE_MSG_TYPE_CONNACK:
		ses->_connack_cb = cb;
		break;
	case MQTTE_MSG_TYPE_PUBLISH:
	case MQTTE_MSG_TYPE_PUBACK:
		ses->_puback_cb = cb;
		break;
	case MQTTE_MSG_TYPE_PUBREC:
	case MQTTE_MSG_TYPE_PUBREL:
	case MQTTE_MSG_TYPE_PUBCOMP:
	case MQTTE_MSG_TYPE_SUBSCRIBE:
	case MQTTE_MSG_TYPE_SUBACK:
	case MQTTE_MSG_TYPE_UNSUBSCRIBE:
	case MQTTE_MSG_TYPE_UNSUBACK:
	case MQTTE_MSG_TYPE_PINGREQ:
	case MQTTE_MSG_TYPE_PINGRESP:
	case MQTTE_MSG_TYPE_DISCONNECT:
	case MQTTE_MSG_TYPE_RESERVED2:
	default:
		return -2;
	}
	return 0;
}

static int _insert_utf8_string(uint8_t *buf, const char *str)
{
	uint16_t len = strlen(str);

	buf[0] = len >> 8;
	buf[1] = len & 0xff;
	memcpy(&buf[2], str, len);

	return len + 2;
}

int mqtte_connect(struct mqtte_session *ses,
		  char *username,
		  char *password,
		  bool will_flag,
		  bool will_retain,
		  char *will_topic,
		  char *will_msg,
		  enum mqtte_qos_level will_qos,
		  uint16_t keepalive)
{
	uint32_t tmp32;
	uint32_t remaining_len = 12; /* Minimum remaining (due to var
				      * header) */

	/* Clear the transmit buffer index */
	ses->tx_index = 0;
	if(ses->lock_take && ses->lock_give)
		ses->lock_take(ses->tx_lock);
	{
		/* Set the message type */
		ses->tx_buf[ses->tx_index++]= MQTTE_MSG_TYPE_CONNECT << 4;

		/* Leave room for the encoded payload length */
		ses->tx_index += 4;

		/*** Encode the variable header ***/

		/* Copy in the protocol name + size */
		ses->tx_buf[ses->tx_index++] = 0x00;
		ses->tx_buf[ses->tx_index++] = 0x06;
		memcpy(&ses->tx_buf[ses->tx_index], "MQIsdp", 6);
		ses->tx_index += 6;

		/* Copy in the protocol version */
		ses->tx_buf[ses->tx_index++] = 0x03;

		/* Set the flags */
		uint8_t flags = 0;
		if (username) {
			flags |= 1 << 7;
		}

		if (password) {
			flags |= 1 << 6;
		}

		if (will_retain) {
			flags |= 1 << 5;
		}

		flags |= (will_qos & 0x03) << 3;

		if (will_flag) {
			flags |= 1 << 2;
		}

		ses->tx_buf[ses->tx_index++] = flags;

		/* Set the keepalive timer */
		ses->tx_buf[ses->tx_index++] = keepalive >> 8;
		ses->tx_buf[ses->tx_index++] = keepalive & 0xff;

		/***  Encode the payload ***/
		int tmp_len;
		/* Ensure that there is a valid client ID */
		tmp32 = strlen(ses->client_id);
		if (!tmp32){
			return -1;
		} else {
			/* Copy in the client ID + length */
			tmp_len = _insert_utf8_string(&ses->tx_buf[ses->tx_index],
							   ses->client_id);
			remaining_len += tmp_len;
			ses->tx_index += tmp_len;
		}

		if (will_flag && will_topic && will_msg) {
			/* Copy in the will topic */
			tmp_len = _insert_utf8_string(&ses->tx_buf[ses->tx_index],
							   will_topic);
			remaining_len += tmp_len;
			ses->tx_index += tmp_len;

			/* Copy in the will message */
			tmp_len = _insert_utf8_string(&ses->tx_buf[ses->tx_index],
							   will_msg);
			remaining_len += tmp_len;
			ses->tx_index += tmp_len;
		}

		if (username) {
			tmp_len = _insert_utf8_string(&ses->tx_buf[ses->tx_index],
							   username);
			remaining_len += tmp_len;
			ses->tx_index += tmp_len;
		}

		if (password) {
			tmp_len = _insert_utf8_string(&ses->tx_buf[ses->tx_index],
							   password);
			remaining_len += tmp_len;
			ses->tx_index += tmp_len;
		}

		/* We now know the total remaining length and can
		 * encode the fixed header */

		tmp_len =  mqtte_set_packet_length(&ses->tx_buf[1],
						   remaining_len);

		/* Since we know how many additional bytes were eaten
		 * up by the additional length field, we can pack the
		 * tx buffer */

		/* Copy the var header + payload to the end of the
		 * remaining length field */
		memcpy(&ses->tx_buf[1 + tmp_len],
		       &ses->tx_buf[5], remaining_len);

		/* Decrement the tx index to properly reflect length */
		ses->tx_index -= 4 - tmp_len;

#if defined(DEBUG)
		/* Print the results (for debugging) */
		printf("Connect packet (length %d):\n", ses->tx_index);
		for(int j = 0; j < ses->tx_index; j++) {
			if(ses->tx_buf[j] < 128 && ses->tx_buf[j] > 32)
				printf("%c|", ses->tx_buf[j]);
			else
				printf("%02x|", ses->tx_buf[j]);
			if(j % 10 == 0  && j > 0)
				printf("\n");
		}
		printf("\n");
#endif
		/* Send the packet */
		ses->tx_cb(ses->com_handle, ses->tx_buf, ses->tx_index);
	}
	if(ses->lock_give && ses->lock_take)
		ses->lock_give(ses->tx_lock);

	return 0;
}

int mqtte_publish(struct mqtte_session *ses,
		  const char *topic_name,
		  uint8_t *payload,
		  int payload_len,
		  enum mqtte_qos_level qos,
		  bool dup,
		  bool retain,
		  uint16_t msg_id)
{
	uint32_t remaining_len = 0;
	uint8_t header_b1 = 0;
	int tmp_len = 0;

	/* Make sure there is a topic */
	if (!topic_name)
		return -1;

	/* Clear the transmit buffer index */
	ses->tx_index = 0;
	if(ses->lock_take && ses->lock_give)
		ses->lock_take(ses->tx_lock);
	{
		/* Set the message type */
		header_b1 = MQTTE_MSG_TYPE_PUBLISH << 4;
		header_b1 |= (dup ? 1 : 0) << 3;
		header_b1 |= qos << 1;
		header_b1 |= (retain ? 1 : 0) << 0;
		ses->tx_buf[ses->tx_index++] = header_b1;

		/* Leave room for the encoded payload length */
		ses->tx_index += 4;

		/* Insert the topic name */
		tmp_len = _insert_utf8_string(&ses->tx_buf[ses->tx_index],
					      topic_name);

		ses->tx_index += tmp_len;
		/* Insert the message ID */
		ses->tx_buf[ses->tx_index++] = msg_id >> 8;
		ses->tx_buf[ses->tx_index++] = msg_id & 0xff;

		/*  Set the remaining length by the length of the
		packet to the length of the var header + the length of
		the payload
		*/
		remaining_len = tmp_len + 2 + payload_len;

		/* Set the packet length & figure out how many bytes
		 * are necessary to encode it */
		tmp_len = mqtte_set_packet_length(&ses->tx_buf[1], remaining_len);

		if (tmp_len < 0)
			return -2;

		/* Verify that we can fit all of that into the buffer */
		if (remaining_len + tmp_len + 1 > MQTTE_TX_BUF_SIZE)
			return -3;

		/* Copy in the payload */
		memcpy(&ses->tx_buf[ses->tx_index], payload, payload_len);
		ses->tx_index += payload_len;

		/* Now pack everything together */
		memcpy(&ses->tx_buf[1 + tmp_len], &ses->tx_buf[5],
			remaining_len);

		/* Decrement tx_index to reflect actual packet size */
		ses->tx_index -= 4 - tmp_len;

#if defined(DEBUG)
		/* Print the results (for debugging) */
		printf("PUBLISH packet (length %d):\n", ses->tx_index);
		for(int j = 0; j < ses->tx_index; j++) {
			if(ses->tx_buf[j] < 128 && ses->tx_buf[j] > 32)
				printf("'%c'|", ses->tx_buf[j]);
			else
				printf("%02x|", ses->tx_buf[j]);
			if(j % 10 == 0  && j > 0)
				printf("\n");
		}
		printf("\n");
#endif

		/* Send the packet */
		ses->tx_cb(ses->com_handle, ses->tx_buf, ses->tx_index);

	}
	if(ses->lock_take && ses->lock_give)
		ses->lock_give(ses->tx_lock);

	return 0;
}

int mqtte_set_client_id(struct mqtte_session *ses,
			const char *id)
{
	memset(ses->client_id, 0x00, 23);

	int strl = strlen(id);

	if (strl > 22)
		return -1;

	memcpy(ses->client_id, id, strl);

	return 0;
}

static void mqtte_handle_connack(struct mqtte_session *ses)
{
	/* For now, if the user has a connack CB registered, call it
	 * with the result as an argument */
	/* TODO: Return something to ensure we can properly debug this */
	if(ses->_connack_cb)
		ses->_connack_cb(ses->rx_buf[3]);
}

static void mqtte_handle_puback(struct mqtte_session *ses)
{
	/* TODO: Return something to ensure we can properly debug this */
	if(ses->_puback_cb)
		ses->_puback_cb((ses->rx_buf[2] << 8) | ses->rx_buf[3]);
}


static void mqtte_handle_rx_packet(struct mqtte_session *ses)
{
	/* Figure out what kind of packet we're dealing with */
	switch(ses->rx_buf[0]  >> 4) {
	case MQTTE_MSG_TYPE_CONNECT:
		return;
	case MQTTE_MSG_TYPE_CONNACK:
		mqtte_handle_connack(ses);
		return;
	case MQTTE_MSG_TYPE_PUBACK:
		mqtte_handle_puback(ses);
		return;
	default:
		#define DEBUG 1
#if defined (DEBUG)
		printf("Unhandled Msg: %x\n", ses->rx_buf[0]);
#endif
		return;
	}
}

void mqtte_start_session(struct mqtte_session *ses,
			 int com_handle,
			 int(*com_tx)(int, uint8_t*, int),
			 int(*com_rx)(int, uint8_t*, int))
{
	ses->com_handle = com_handle;
	ses->tx_cb = com_tx;
	ses->rx_cb = com_rx;
}

void mqtte_poll(struct mqtte_session *ses)
{
	int pkt_len;
	int ret;

	if (ses->rx_required) {
		/* Attempt to read the desired amount of data */
		ret = ses->rx_cb(ses->com_handle,
				 &ses->rx_buf[ses->rx_index],
				 ses->rx_required);

		ses->rx_required -= ret;
		ses->rx_index += ret;

		if(ses->rx_required)
			return;
	}

	switch (ses->_rx_state) {
	case MQTTE_RX_INIT:
		ses->rx_index = 0;
		ses->rx_required = 2;
		ses->_rx_state = MQTTE_RX_SEEKING_LEN;
		return;
	case MQTTE_RX_SEEKING_LEN:
		pkt_len = mqtte_get_packet_length(&ses->rx_buf[1]);
		/* If we aren't able to determine packet length, read
		 * another byte the next time around */
		if(pkt_len == -1) {
			ses->rx_required++;
			return;
		}

		/* Otherwise, set our target length */
		ses->rx_required = pkt_len;
		ses->_rx_state = MQTTE_RX_RECEIVING;
		break;
	case MQTTE_RX_RECEIVING:
		/* If we get here, we've received our complete
		 * packet, time to decode and handle that bad boy */
		mqtte_handle_rx_packet(ses);

		/* Clear out our RX buffer */
		memset(ses->rx_buf, 0xff, MQTTE_RX_BUF_SIZE);

		/* Set our state to INIT and bail out */
		ses->_rx_state = MQTTE_RX_INIT;
		return;
	default:
		while(1);
		/* This is an error, we shouldn't get here */
	}
}
